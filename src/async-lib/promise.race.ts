//--------------------------------------------------
// Doesn't work!
// export async function race(promises) {
//     const results = [];
//     for (const p of promises) {
//         results.push(await p);
//     }
//     //   promises.forEach(async p => {
//     //     results.push(await p);
//     //   });

//     return await results[0];
// }
//--------------------------------------------------
// you have to use the Promise object
export async function race(promises: any[] | Promise<any>[]): Promise<any> {
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            resolve(await p);
        });
    });
}
