//--------------------------------------------------
// same here - you have to use the Promise object
export async function some(
    promises: any[] | Promise<any>[],
    num: number
): Promise<any[]> {
    const results = [] as any[];
    return await new Promise((resolve) => {
        promises.forEach(async (p) => {
            results.push(await p);
            if (results.length === num) resolve(results);
        });
    });
}
