import { iterable, cb } from "./promise.types";
//--------------------------------------------------
export async function each(
    iterable: iterable,
    cb: cb
): Promise<any[] | string> {
    for (const item of iterable) {
        await cb(item);
    }
    return iterable;
}

//--------------------------------------------------
