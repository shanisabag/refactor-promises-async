export type iterable = string | any[] | Promise<any>[];
export type cb = (
    item: any | Promise<any>,
    index?: number,
    arrLength?: number
) => Promise<any>;
export type cbFilter = (
    item: any | Promise<any>,
    index?: number,
    arrLength?: number
) => Promise<boolean>;
export type cbReduce = (
    acc: any,
    item: any | Promise<any>,
    index?: number,
    arrLength?: number
) => Promise<any>;
export interface IPropsObj {
    [key: string]: any | Promise<any>;
}
