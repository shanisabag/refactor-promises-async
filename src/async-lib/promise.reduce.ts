import { cbReduce, iterable } from "./promise.types";

export async function reduce(
    iterable: iterable,
    cb: cbReduce,
    initial?: any
): Promise<any> {
    let aggregator = initial !== undefined ? initial : iterable[0];

    for (const item of iterable) {
        aggregator = await cb(aggregator, item);
    }

    return aggregator;
}
