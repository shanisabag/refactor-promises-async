import { IPropsObj } from "./promise.types";

export async function props(promisesObj: IPropsObj): Promise<IPropsObj> {
    const results = {} as IPropsObj;
    for (const key in promisesObj) {
        results[key] = await promisesObj[key];
    }
    return results;
}
