# Node.js TS
## This is a brand new Promises Async reimplementation in TypeScript
Implements the following functions:
1. all
2. props
3. each
4. map (mapParallel and mapSeries)
5. filter (filterParallel and filterSeries)
6. reduce
7. race
8. some