import { expect } from "chai";
import { describe } from "mocha";
import { props } from "../src/async-lib/promise.props";
import { echo } from "../src/async-lib/promise.utils";
import { IPropsObj } from "../src/async-lib/promise.types";

describe("The Promise.props module", () => {
    context("props", () => {
        let pendingPromises : IPropsObj;
        before(() => {
            const promise1 = echo("1 first resolved value", 100);
            const promise2 = echo("2 second resolved value", 100);
            const promise3 = echo("3 third resolved value", 100);
            
            pendingPromises = {a: promise1, b: promise2, c: promise3};
        });

        it("should exist", () => {
            expect(props).to.be.instanceOf(Function);
        });

        it("should resolve all promises", async () => {
            const actual = await props(pendingPromises);
            expect(actual).to.eql({
                a: "1 first resolved value",
                b: "2 second resolved value",
                c: "3 third resolved value"
            });
        });
    });
});