import { expect } from "chai";
import { describe } from "mocha";
import { race } from "../src/async-lib/promise.race";
import { echo } from "../src/async-lib/promise.utils";

describe("The Promise.race module", () => {
    context("race", () => {
        let pendingPromises : Promise<any>[];
        before(() => {
            const promise1 = echo("first", 300);
            const promise2 = echo("second", 100);
            const promise3 = echo("third", 600);
            
            pendingPromises = [promise1, promise2, promise3];
        });

        it("should exist", () => {
            expect(race).to.be.instanceOf(Function);
        });

        it("should resolve a promise that fulfills or rejects as soon as one of the promises in an iterable fulfills or rejects", async () => {
            const actual = await race(pendingPromises);
            expect(actual).to.equal("second");
        });
    });
});