import { expect } from "chai";
import { describe } from "mocha";
import { each } from "../src/async-lib/promise.each";
import { delay, random } from "../src/async-lib/promise.utils";

describe("The Promise.each module", () => {
    context("each", () => {
        it("should exist", () => {
            expect(each).to.be.instanceOf(Function);
        });

        it("should resolve each promises", async () => {
            const actual = await each("Geronimo", async (char) => {
                        console.log(`${char} -->`);
                        await delay(random(500, 100));
                        console.log(`<-- ${char}`);
                        return char.toUpperCase(); // No effect...
                    });
            expect(actual).to.equal("Geronimo");
        });
    });
});