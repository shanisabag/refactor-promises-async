import { expect } from "chai";
import { describe } from "mocha";
import { all } from "../src/async-lib/promise.all";
import { echo } from "../src/async-lib/promise.utils";

describe("The Promise.all module", () => {
    context("all", () => {
        let pendingPromises : Promise<any>[];
        before(() => {
            const promise1 = echo("1 first resolved value", 100);
            const promise2 = echo("2 second resolved value", 100);
            const promise3 = echo("3 third resolved value", 100);
            
            pendingPromises = [promise1, promise2, promise3];
        });

        it("should exist", () => {
            expect(all).to.be.instanceOf(Function);
        });

        it("should resolve all promises", async () => {
            const actual = await all(pendingPromises);
            expect(actual).to.eql([
                "1 first resolved value",
                "2 second resolved value",
                "3 third resolved value"
            ]);
        });
    });
});