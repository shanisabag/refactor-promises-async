import { expect } from "chai";
import { describe } from "mocha";
import { some } from "../src/async-lib/promise.some";
import { echo } from "../src/async-lib/promise.utils";

describe("The Promise.some module", () => {
    context("some", () => {
        let pendingPromises : Promise<any>[];
        before(() => {
            const promise1 = echo("first", 300);
            const promise2 = echo("second", 100);
            const promise3 = echo("third", 600);
            const promise4 = echo("forth", 400);
            
            pendingPromises = [promise1, promise2, promise3, promise4];
        });

        it("should exist", () => {
            expect(some).to.be.instanceOf(Function);
        });

        it("should resolve a promise that is fulfilled as soon as count promises are fulfilled in the array", async () => {
            const actual = await some(pendingPromises, 2);
            expect(actual).to.eql(["second", "first"]);
        });
    });
});