import { expect } from "chai";
import { describe } from "mocha";
import { mapParallel, mapSeries } from "../src/async-lib/promise.map";
import { delay, random } from "../src/async-lib/promise.utils";

describe("The Promise.map module", () => {
    context("mapParallel", () => {
        it("should exist", () => {
            expect(mapParallel).to.be.instanceOf(Function);
        });

        it("should map all promises", async () => {
            const actual = await mapParallel("Geronimo", async (char) => {
                        console.log(`${char} -->`);
                        await delay(random(500, 100));
                        console.log(`<-- ${char}`);
                        return char.toUpperCase(); // Modify each item in the iterable
                    });
            expect(actual).to.eql(["G", "E", "R", "O", "N", "I", "M", "O"]);
        });
    });

    context("mapSeries", () => {
        it("should exist", () => {
            expect(mapSeries).to.be.instanceOf(Function);
        });

        it("should map all promises", async () => {
            const actual = await mapSeries("Geronimo", async (char) => {
                        console.log(`${char} -->`);
                        await delay(random(500, 100));
                        console.log(`<-- ${char}`);
                        return char.toUpperCase(); // Modify each item in the iterable
                    });
            expect(actual).to.equal("GERONIMO");
        });
    });
});