import { expect } from "chai";
import { describe } from "mocha";
import { reduce } from "../src/async-lib/promise.reduce";
import { delay, random } from "../src/async-lib/promise.utils";

describe("The Promise.reduce module", () => {
    context("reduce", () => {
        it("should exist", () => {
            expect(reduce).to.be.instanceOf(Function);
        });

        it("should reduce the array to a value using the given reducer function", async () => {
            const actual = await reduce(
                [51, 64, 25, 12, 93],
                async (total, num) => {
                    console.log(`${num} -->`);
                    await delay(random(400, 100));
                    console.log(`<-- ${num}`);
                    return total + num;
                },
                0
            );
            expect(actual).to.equal(245);
        });
    });
});