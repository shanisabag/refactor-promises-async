import { expect } from "chai";
import { describe } from "mocha";
import { filterParallel, filterSeries } from "../src/async-lib/promise.filter";
import { delay, random } from "../src/async-lib/promise.utils";

describe("The Promise.filter module", () => {
    context("filterParallel", () => {
        it("should exist", () => {
            expect(filterParallel).to.be.instanceOf(Function);
        });

        it("should filter all promises", async () => {
            const actual = await filterParallel("G<4!e3ro0ni1mo", async (char) => {
                console.log(`${char} -->`);
                await delay(random(500, 100));
                console.log(`<-- ${char}`);
                return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
        });
            expect(actual).to.equal("Geronimo");
        });
    });

    context("filterSeries", () => {
        it("should exist", () => {
            expect(filterSeries).to.be.instanceOf(Function);
        });

        it("should filter all promises", async () => {
            const actual = await filterSeries("G<4!e3ro0ni1mo", async (char) => {
                console.log(`${char} -->`);
                await delay(random(500, 100));
                console.log(`<-- ${char}`);
                return /^[A-Za-z]+$/.test(char); // test for alphabetic characters
        });
            expect(actual).to.equal("Geronimo");
        });
    });
});