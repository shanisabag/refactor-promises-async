import { iterable, cb } from "./promise.types";
export declare function mapParallel(iterable: iterable, cb: cb): Promise<any[]>;
export declare function mapSeries(iterable: iterable, cb: cb): Promise<string | any[]>;
