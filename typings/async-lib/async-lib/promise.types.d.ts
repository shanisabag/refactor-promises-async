export declare type iterable = string | any[] | Promise<any>[];
export declare type cb = (item: any | Promise<any>, index?: number, arrLength?: number) => Promise<any>;
export declare type cbFilter = (item: any | Promise<any>, index?: number, arrLength?: number) => Promise<boolean>;
export declare type cbReduce = (acc: any, item: any | Promise<any>, index?: number, arrLength?: number) => Promise<any>;
export interface IPropsObj {
    [key: string]: any | Promise<any>;
}
