import { iterable } from "./promise.types";
export declare function all(promises: iterable): Promise<any[] | string>;
