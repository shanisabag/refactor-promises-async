import { iterable, cbFilter } from "./promise.types";
export declare function filterSeries(iterable: iterable, cb: cbFilter): Promise<string | any[]>;
export declare function filterParallel(iterable: iterable, cb: cbFilter): Promise<string | any[]>;
