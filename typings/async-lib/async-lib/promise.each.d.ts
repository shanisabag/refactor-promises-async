import { iterable, cb } from "./promise.types";
export declare function each(iterable: iterable, cb: cb): Promise<any[] | string>;
