import { IPropsObj } from "./promise.types";
export declare function props(promisesObj: IPropsObj): Promise<IPropsObj>;
