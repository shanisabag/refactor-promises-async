import { cbReduce, iterable } from "./promise.types";
export declare function reduce(iterable: iterable, cb: cbReduce, initial?: any): Promise<any>;
